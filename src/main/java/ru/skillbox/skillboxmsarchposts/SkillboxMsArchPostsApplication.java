package ru.skillbox.skillboxmsarchposts;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SkillboxMsArchPostsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SkillboxMsArchPostsApplication.class, args);
	}

}
